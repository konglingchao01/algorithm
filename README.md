# Algorithm


1. [基于Vue的物联网可视化平台，同时集成了安全预警系统。](https://github.com/LeBronChao/Big-Data-Visualization)


2.[风险识别与预测赛题](https://github.com/yzkang/QLM-Tianchi)
模型方案

![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/111346_8d73b317_9109293.png "屏幕截图.png")




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
